## 64位 AI2(App Inventor 2)离线版服务器 单机版
https://gitee.com/fsyz/README/blob/master/AI2Local/README.md   
### 介绍
我们的目标：搭建一个本地多用户的App Inventor 2 服务器   
目的：课堂教学，社团活动，兴趣学习   
优势：管理权限（用户管理，账号切换，资源打包），网络链接速度快，拥有配套服务。   
注意：每次退出前导出自己的项目到本地做备份。   
项目说明： https://gitee.com/fsyz/README   
### 软件版本:
App Inventor 2版本：     编译版本：v185a-1086-gec1e463c9 Use Companion: 使用AI伴侣：2.73a 或者2.73au  
　　　　　　　　　　　　　　　　　　　　　　https://github.com/mit-cml/appinventor-sources   
OPENJDK版本 11.0.24   https://mirrors.tuna.tsinghua.edu.cn/Adoptium/11/jdk/x64/windows/   
　　　　　　　　　　　　   
google-cloud-sdk版本：493.0.0    https://cloud.google.com/sdk/docs/downloads-interactive   
   
### 单机个人版特点：
1.同步官方最新版本，没有对java源代码进行修改，只修改DevServer\login.jsp及\DevServer\WEB-INF\appengine-web.xml 2个文件；  
2.无需联网外网就能使用；  
3.带有桌面汉化版AI伴侣；   
4.绿色软件，直接运行bat就可以使用,使用过程中不能关闭bat界面；  
5.可以本地将AIA文件编译为APK.  https://gitee.com/fsyz/README/blob/master/AI2server/BuildApk.md  
6.修正win服务器下有时提示项目不能保存的问题 https://gte.fsyz.net/node/1943   
### 使用方法：
详见： https://gitee.com/fsyz/README/blob/master/AI2Local/README.md   
演示视频： https://www.bilibili.com/video/bv1C3411V7Zu   
