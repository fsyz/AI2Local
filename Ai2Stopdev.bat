@echo off

rem  port 8888 
for /f "tokens=1-5" %%i in ('netstat -ano^|findstr ":8888.*LISTENING"') do (
    echo kill the process %%m  use the port 8888
    taskkill /F /T /pid %%m
)