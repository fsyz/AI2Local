@echo off  
rem %1(start /min cmd.exe /c %0 :&exit)
chcp 65001
echo "Starting App Inventor 2 DevAppServer..."
title DevAppServer
cd /d %~dp0

SET JAVA_HOME=%~dp0OPENJDK
rem SET PYTHON_HOME=%~dp0Python
SET Cloud_SDK_HOME=%~dp0GCloudSdk
SET PATH=%JAVA_HOME%\bin;%Cloud_SDK_HOME%\bin;%PATH%;

SET JAVA_TOOL_OPTIONS="-Dfile.encoding=UTF-8"

for /f "tokens=1-5" %%i in ('netstat -ano^|findstr ":8888"') do (
    echo kill the process %%m  use the port 8888
    taskkill /F /T /pid %%m
)

java_dev_appserver.cmd  --port=8888 --disable_update_check --address=0.0.0.0 DevServer
