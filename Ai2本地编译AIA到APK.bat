@echo off  
chcp 65001
cd /d %~dp0
echo "准备将AIA编译为APK,要输入用户名(邮箱)和AIA文件名" 

SET "email=%1"
if "%email%"=="" set /p "email=1.请输入用户名(email)： " || set "email=test@test.com"

SET userName=%email%

set "inputfile=%2"
if "%inputfile%"=="" set /p "inputfile=2.请输入需要编译的aia文件(Tab键会补全文件名)："

SET inputfile=%~dp0\%inputfile%

SET outputDir=%~dp0
SET dexcache=%~dp0dexcache
SET JAVA_HOME=%~dp0OPENJDK
rem SET PYTHON_HOME=%~dp0Python
SET BUILD_HOME=%~dp0BuildServer
SET Cloud_SDK_HOME=%~dp0GCloudSdk
SET PATH=%JAVA_HOME%\bin; %Cloud_SDK_HOME%\bin;%PATH%;
if  exist %dexcache% ( 
  cd %dexcache% 
  del  /q  "*.jar"
  cd ..
)
SET _JAVA_OPTIONS= -Xms64m -Xmx1024m
SET JAVA_TOOL_OPTIONS="-Dfile.encoding=UTF-8"

cd "%BUILD_HOME%"

java -cp "*" com.google.appinventor.buildserver.Main --inputZipFile %inputfile% --userName %username% --outputDir %outputdir% --dexCacheDir %dexcache%
cd %outputDir%

echo 按任意键退出...
pause 