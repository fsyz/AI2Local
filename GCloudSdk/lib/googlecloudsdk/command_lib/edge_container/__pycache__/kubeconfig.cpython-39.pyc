a
    ���C  �                	   @   s0  d Z ddlmZ ddlmZ ddlmZ ddlZddlZddlmZ ddlm	Z
 ddlmZ dd	lmZ dd
lmZ ddlmZ ddlmZ ddlmZ dZdZG dd� de
j�ZG dd� de�Zdd� Zdd� Zdd� Zdd� ZG dd� de�Zd,dd�Zd-d d!�Zd"Zd#d$� Z d.d&d'�Z!d(d)� Z"d*d+� Z#dS )/z,Utilities for generating kubeconfig entries.�    )�absolute_import)�division)�unicode_literalsN)�config)�
exceptions)�log)�
properties)�yaml)�encoding)�files)�	platformsa!  
Fetch credentials for a running {kind} cluster.
This command updates a kubeconfig file with appropriate credentials and
endpoint information to point kubectl at a specific {kind} cluster.
By default, credentials are written to ``HOME/.kube/config''.
You can provide an alternate path by setting the ``KUBECONFIG'' environment
variable. If ``KUBECONFIG'' contains multiple paths, the first one is used.
This command enables switching to a specific cluster, when working
with multiple clusters. It can also be used to access a previously created
cluster from a new workstation.
The command will configure kubectl to automatically refresh its
credentials using the same identity as the gcloud command-line tool.
See [](https://cloud.google.com/kubernetes-engine/docs/kubectl) for
kubectl documentation.
z�
To get credentials of a cluster named ``my-cluster'' managed in location ``us-west1'',
run:
$ {command} my-cluster --location=us-west1
c                   @   s   e Zd ZdZdS )�Errorz>Class for errors raised by edgecontainer kubeconfig utilities.N��__name__�
__module__�__qualname__�__doc__� r   r   �;lib\googlecloudsdk\command_lib\edge_container\kubeconfig.pyr   6   s   r   c                   @   s   e Zd ZdZdS )�MissingEnvVarErrorzDAn exception raised when required environment variables are missing.Nr   r   r   r   r   r   :   s   r   c                 C   s   d}|j | ||d�S )a  Generates a kubeconfig context for a Edge Container cluster.

  Args:
    project_id: str, project ID associated with the cluster.
    location: str, Google location of the cluster.
    cluster_id: str, ID of the cluster.

  Returns:
    The context for the kubeconfig entry.
  z2edgecontainer_{project_id}_{location}_{cluster_id})�
project_id�location�
cluster_id��format)r   r   r   �templater   r   r   �GenerateContext>   s    �r   c                 C   s   d}|j | |||d�S )aW  Generates command arguments for kubeconfig's authorization provider.

  Args:
    track: str, command track to use.
    cluster_id: str, ID of the cluster.
    project_id: str, ID of the project of the cluster.
    location: str, Google location of the cluster.

  Returns:
    The command arguments for kubeconfig's authorization provider.
  zl{track} edge-container clusters print-access-token {cluster_id} --project={project_id} --location={location})�trackr   r   r   r   )r   r   r   r   r   r   r   r   �GenerateAuthProviderCmdArgsN   s    �r   c                 C   s�   t �� }t|||�|j|< d||ddd�}t|fi |��}||j|< i }| jdu r^t�d� nt	| j�|d< t
|d�| j�fi |��|j|< |�|� |��  tj�d	�|�� dS )
ah  Generates a kubeconfig entry for a Edge Container cluster.

  Args:
    cluster: object, Edge Container cluster.
    context: str, context for the kubeconfig entry.
    cmd_path: str, authentication provider command path.
    cmd_args: str, authentication provider command arguments.

  Raises:
      Error: don't have the permission to open kubeconfig file
  �gcpz{.expireTime}z{.accessToken})�auth_provider�auth_provider_cmd_path�auth_provider_cmd_args�auth_provider_expiry_key�auth_provider_token_keyNz.Cluster is missing certificate authority data.�ca_datazhttps://{}:6443zNA new kubeconfig entry "{}" has been generated and set as the current context.)�
Kubeconfig�Default�Context�contexts�User�users�clusterCaCertificater   �warning�
_GetCaData�Clusterr   �endpoint�clusters�SetCurrentContext�
SaveToFile�status�Print)�cluster�context�cmd_path�cmd_args�
kubeconfig�user_kwargs�user�cluster_kwargsr   r   r   �GenerateKubeconfigc   s4    �

��
��r>   c                 C   s   t �| �d���d�S )Nzutf-8)�base64�	b64encode�encode�decode)�pemr   r   r   r.   �   s    r.   c                   @   s�   e Zd ZdZdd� Zedd� �Zedd� �Zdd	� Zd
d� Z	dd� Z
edd� �Zedd� �Zedd� �Zedd� �Zedd� �Zdd� ZdS )r&   z1Interface for interacting with a kubeconfig file.c                 C   s|   || _ || _i | _i | _i | _| jd D ]}|| j|d < q(| jd D ]}|| j|d < qF| jd D ]}|| j|d < qdd S )Nr1   �namer+   r)   )�	_filename�_datar1   r+   r)   )�self�raw_data�filenamer6   r<   r7   r   r   r   �__init__�   s    zKubeconfig.__init__c                 C   s
   | j d S �N�current-context�rF   �rG   r   r   r   �current_context�   s    zKubeconfig.current_contextc                 C   s   | j S �N)rE   rN   r   r   r   rI   �   s    zKubeconfig.filenamec                 C   sH   | j �|d � | j�|d � | j�|d � | j�d�|krDd| jd< d S )NrL   � )r)   �popr1   r+   rF   �get)rG   �keyr   r   r   �Clear�   s
    zKubeconfig.Clearc                 C   s~   t | j�� �| jd< t | j�� �| jd< t | j�� �| jd< tj| jdd��}t	�
| j|� W d  � n1 sp0    Y  dS )zjSave kubeconfig to file.

    Raises:
      Error: don't have the permission to open kubeconfig file.
    r1   r+   r)   T)�privateN)�listr1   �valuesrF   r+   r)   �
file_utils�
FileWriterrE   r	   �dump)rG   �fpr   r   r   r3   �   s
    zKubeconfig.SaveToFilec                 C   s   || j d< d S rK   rM   )rG   r7   r   r   r   r2   �   s    zKubeconfig.SetCurrentContextc              
   C   s|   |st d��z8dD ].}t|| t�st d�|| t|| ����qW n2 tyv } zt d�|���W Y d}~n
d}~0 0 dS )z2Make sure we have the main fields of a kubeconfig.z
empty file)r1   r+   r)   zinvalid type for {0}: {1}zexpected key {0} not foundN)r   �
isinstancerW   r   �type�KeyError)�cls�datarT   �errorr   r   r   �	_Validate�   s    �zKubeconfig._Validatec              
   C   s\   zt �|�}W n8 t jyF } ztd�||j���W Y d }~n
d }~0 0 | �|� | ||�S )Nz&unable to load kubeconfig for {0}: {1})r	   �	load_pathr   r   �inner_errorrc   )r`   rI   ra   rb   r   r   r   �LoadFromFile�   s    �
zKubeconfig.LoadFromFilec              
   C   sv   z| � |�W S  ttfyp } zHt�d�||�� t�tj	�
|�� | t� |�}|��  |W  Y d}~S d}~0 0 dS )zARead in the kubeconfig, and if it doesn't exist create one there.z6unable to load default kubeconfig: {0}; recreating {1}N)rf   r   �IOErrorr   �debugr   rY   �MakeDir�os�path�dirname�EmptyKubeconfigr3   )r`   rI   rb   r:   r   r   r   �LoadOrCreate�   s    �zKubeconfig.LoadOrCreatec                 C   s   | � t�� �S rP   )rn   r&   �DefaultPath)r`   r   r   r   r'   �   s    zKubeconfig.Defaultc                  C   s�   t �tjd�} | r.| �tj�d } tj�| �S t �tjd�}|s�tj	�
� r�t �tjd�}t �tjd�}|r||r|tj�||�}|s�t �tjd�}|s�tdjtj	�
� r�dndd	���tj�|d
d�S )z(Return default path for kubeconfig file.�
KUBECONFIGr   �HOME�	HOMEDRIVE�HOMEPATH�USERPROFILEzVenvironment variable {vars} or KUBECONFIG must be set to store credentials for kubectlz&HOMEDRIVE/HOMEPATH, USERPROFILE, HOME,)�varsz.kuber   )r
   �GetEncodedValuerj   �environ�split�pathseprk   �abspathr   �OperatingSystem�	IsWindows�joinr   r   )r:   �home_dir�
home_drive�	home_pathr   r   r   ro   �   s(    ��zKubeconfig.DefaultPathc                 C   s|   | � | jp|j� tt|j�� �t| j�� � �| _tt|j�� �t| j�� � �| _tt|j�� �t| j�� � �| _dS )z�Merge another kubeconfig into self.

    In case of overlapping keys, the value in self is kept and the value in
    the other kubeconfig is lost.

    Args:
      kubeconfig: a Kubeconfig instance
    N)r2   rO   �dictrW   r1   �itemsr+   r)   )rG   r:   r   r   r   �Merge  s    	�"�zKubeconfig.MergeN)r   r   r   r   rJ   �propertyrO   rI   rU   r3   r2   �classmethodrc   rf   rn   r'   �staticmethodro   r�   r   r   r   r   r&   �   s(   



	


r&   c                 C   sF   d|i}|r|rt d��|r&||d< n|r4||d< nd|d< | |d�S )z0Generate and return a cluster kubeconfig object.�serverz'cannot specify both ca_path and ca_datazcertificate-authorityzcertificate-authority-dataTzinsecure-skip-tls-verify)rD   r6   )r   )rD   r�   �ca_pathr%   r6   r   r   r   r/     s    �

r/   c
                 C   s�   |s|r|s|r|	st d��i }
d}t�tjd�}|dkr>d}|rx|sV|sV|sV|sV|snt|||||d�|
d< n
t� |
d< |r�|r�t d	��|r�||
d
< n|r�||
d< |r�|	r�t d��|r�||
d< n|	r�|	|
d< | |
d�S )a  Generates and returns a user kubeconfig object.

  Args:
    name: str, nickname for this user entry.
    auth_provider: str, authentication provider.
    auth_provider_cmd_path: str, authentication provider command path.
    auth_provider_cmd_args: str, authentication provider command args.
    auth_provider_expiry_key: str, authentication provider expiry key.
    auth_provider_token_key: str, authentication provider token key.
    cert_path: str, path to client certificate file.
    cert_data: str, base64 encoded client certificate data.
    key_path: str, path to client key file.
    key_data: str, base64 encoded client key data.

  Returns:
    dict, valid kubeconfig user entry.

  Raises:
    Error: if no auth info is provided (auth_provider or cert AND key)
  z3either auth_provider or cert & key must be providedF�USE_GKE_GCLOUD_AUTH_PLUGIN�TrueT)rD   r8   r9   �
expiry_key�	token_keyzauth-provider�execz+cannot specify both cert_path and cert_datazclient-certificatezclient-certificate-dataz)cannot specify both key_path and key_dataz
client-keyzclient-key-data)rD   r<   )r   r
   rv   rj   rw   �_AuthProvider�_ExecAuthPlugin)rD   r    r!   r"   r#   r$   �	cert_path�	cert_data�key_path�key_datar<   �use_exec_auth�use_gke_gcloud_auth_pluginr   r   r   r*   #  sH    ����


r*   z�Path to sdk installation not found. Please check your installation or use the
`--auth-provider-cmd-path` flag to provide the path to gcloud manually.c                  C   s`   d} t j�� rd} | }t�� j}|dur6tj�|| �}|dddd�}t	j
jj�� r\dg|d	< |S )
ai  Generate and return an exec auth plugin config.

  Constructs an exec auth plugin config entry readable by kubectl.
  This tells kubectl to call out to gke-gcloud-auth-plugin and
  parse the output to retrieve access tokens to authenticate to
  the kubernetes master.

  Kubernetes GKE Auth Provider plugin is defined at
  https://kubernetes.io/docs/reference/access-authn-authz/authentication/#client-go-credential-plugins

  GKE GCloud Exec Auth Plugin code is at
  https://github.com/kubernetes/cloud-provider-gcp/tree/master/cmd/gke-gcloud-auth-plugin

  Returns:
    dict, valid exec auth plugin config entry.
  zgke-gcloud-auth-pluginzgke-gcloud-auth-plugin.exeNz$client.authentication.k8s.io/v1beta1z[Install gke-gcloud-auth-plugin by running: gcloud components install gke-gcloud-auth-pluginT)�command�
apiVersion�installHint�provideClusterInfoz%--use_application_default_credentials�args)r   r{   r|   r   �Paths�sdk_bin_pathrj   rk   r}   r   �VALUES�	container�use_app_default_credentials�GetBool)�bin_namer�   r�   �exec_cfgr   r   r   r�   r  s    

�
r�   r   c           	      C   s�   d| i}| dkr�d}t j�� r"d}|du rRt�� j}|du rDtt��tj	�
||�}||r\|nd|rf|nd|rp|ndd	�}||d
< |S )a�  Generates and returns an auth provider config.

  Constructs an auth provider config entry readable by kubectl. This tells
  kubectl to call out to a specific gcloud command and parse the output to
  retrieve access tokens to authenticate to the kubernetes master.
  Kubernetes gcp auth provider plugin at
  https://github.com/kubernetes/kubernetes/tree/master/staging/src/k8s.io/client-go/plugin/pkg/client/auth/gcp

  Args:
    name: auth provider name
    cmd_path: str, authentication provider command path.
    cmd_args: str, authentication provider command arguments.
    expiry_key: str, authentication provider expiry key.
    token_key: str, authentication provider token key.

  Returns:
    dict, valid auth provider config entry.
  Raises:
    Error: Path to sdk installation not found. Please switch to application
    default credentials using one of

    $ gcloud config set container/use_application_default_credentials true
    $ export CLOUDSDK_CONTAINER_USE_APPLICATION_DEFAULT_CREDENTIALS=true.
  rD   r   �gcloudz
gcloud.cmdNz"config config-helper --format=jsonz{.credential.access_token}z{.credential.token_expiry})zcmd-pathzcmd-argsz	token-keyz
expiry-keyr   )r   r{   r|   r   r�   r�   r   �SDK_BIN_PATH_NOT_FOUNDrj   rk   r}   )	rD   r8   r9   r�   r�   �providerr�   r�   �cfgr   r   r   r�   �  s"    




�r�   c                 C   s   | ||d�d�S )z0Generate and return a context kubeconfig object.)r6   r<   )rD   r7   r   )rD   r6   r<   r   r   r   r(   �  s
    ��r(   c                   C   s   dg g ddi g d�S )N�v1rQ   �Config)r�   r)   r1   rL   �kind�preferencesr+   r   r   r   r   r   rm   �  s    �rm   )NN)	NNNNNNNNN)r   NNNN)$r   �
__future__r   r   r   r?   rj   �googlecloudsdk.corer   r   �core_exceptionsr   r   r	   �googlecloudsdk.core.utilr
   r   rY   r   ZCOMMAND_DESCRIPTIONZCOMMAND_EXAMPLEr   r   r   r   r>   r.   �objectr&   r/   r*   r�   r�   r�   r(   rm   r   r   r   r   �<module>   sX   & 
         �
J&     �
?