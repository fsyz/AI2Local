@echo off
%1(start /min cmd.exe /c %0 :&exit)
chcp 65001
echo "Starting aiStarter..."

title aiStarter

taskkill /F /T /IM nw.exe

SET JAVA_HOME=%~dp0OPENJDK
rem SET PYTHON_HOME=%~dp0Python
SET BUILD_HOME=%~dp0BuildServer
SET Cloud_SDK_HOME=%~dp0google-cloud-sdk
SET dexcache=%~dp0dexcache
SET PATH=%JAVA_HOME%\bin; %Cloud_SDK_HOME%\bin;%PATH%;

cd /d %~dp0
cd Companion

start nw.exe --load-extension=ARChon --user-data-dir=%~dp0Companion\user_data --ignore-gpu-blacklist --enable-gcm  AINEW_USB
