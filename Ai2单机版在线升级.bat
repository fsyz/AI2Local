@echo off  
cd /d %~dp0

ren Git Gitb
if not exist .git ( 
    
    Gitb\bin\git init 
    Gitb\bin\git config --unset http.proxy

    Gitb\bin\git config --unset https.proxy

    Gitb\bin\git remote add origin https://gitee.com/fsyz/AI2Local.git
    Gitb\bin\git fetch origin
    Gitb\bin\git branch master origin/master
    Gitb\bin\git pull
    Gitb\bin\git reset --hard
     
  
    
) else (

  Gitb\bin\git config --unset http.proxy

  Gitb\bin\git config --unset https.proxy

  Gitb\bin\git fetch origin master

  Gitb\bin\git reset --hard origin/master

)

if not exist Git (
        ren Gitb Git
    ) else (
        rd /s /q Gitb 
  )
 
pause
