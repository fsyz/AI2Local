@echo off

rem  port 9990 
for /f "tokens=1-5" %%i in ('netstat -ano^|findstr ":9990"') do (
    echo kill the process %%m  use the port 9990
    taskkill /F /T /pid %%m
)
