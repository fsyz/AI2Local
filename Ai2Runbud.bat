@echo off
%1(start /min cmd.exe /c %0 :&exit)
chcp 65001
echo "Starting App Inventor 2 BuildServer..." 
title BuildServer
cd /d %~dp0

SET JAVA_HOME=%~dp0OPENJDK
rem SET PYTHON_HOME=%~dp0Python
SET BUILD_HOME=%~dp0BuildServer
SET Cloud_SDK_HOME=%~dp0GCloudSdk
SET dexcache=%~dp0dexcache
SET PATH=%JAVA_HOME%\bin; %Cloud_SDK_HOME%\bin;%PATH%;

SET _JAVA_OPTIONS= -Xms64m -Xmx1024m
SET JAVA_TOOL_OPTIONS="-Dfile.encoding=UTF-8"
if  exist %dexcache% ( 
  cd %dexcache% 
  del  /q  "*.jar"
  cd ..
)
for /f "tokens=1-5" %%i in ('netstat -ano^|findstr ":9990"') do (
    echo kill the process %%m  use the port 9990
    taskkill /F /T /pid %%m
)

cd "%BUILD_HOME%"

 java -cp "*" com.google.appinventor.buildserver.BuildServer --dexCacheDir %dexcache%
